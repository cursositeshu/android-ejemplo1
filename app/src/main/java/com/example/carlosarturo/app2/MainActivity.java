package com.example.carlosarturo.app2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button compute_button=(Button) findViewById(R.id.compute_button);
        final EditText txtHeight=(EditText) findViewById(R.id.txtHeight);
        final EditText txtWeight=(EditText) findViewById(R.id.txtWeith);
        final TextView lblResult=(TextView) findViewById(R.id.lblResult);

        compute_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(txtHeight.getText().length()>0 && txtWeight.getText().length()>0){
                    float ibm=CalculateMBI(Float.parseFloat(txtHeight.getText().toString()),Float.parseFloat(txtWeight.getText().toString()));
                    if(ibm <16)
                    {
                        lblResult.setText("Your EJM: "+ibm+"(severely underweight)");
                    }
                    else if(ibm<18.5)
                    {
                        lblResult.setText("Your EJM: "+ibm+"(Underweight)");
                    }
                    else if(ibm < 25)
                    {
                        lblResult.setText("Your EJM: "+ibm+ "Normal");
                    }
                    else if(ibm < 30)
                    {
                        lblResult.setText("Your EJM: "+ibm+ "Overweight");
                    }
                    else
                    {
                        lblResult.setText("Your EJM: "+ibm+ "(Obese)");
                    }
                }else{
                    Toast.makeText(getApplicationContext(),"Datos faltantes", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private float CalculateMBI(Float heigt, Float weith){
        heigt=(heigt/100);
        return (Float) (weith/(heigt*heigt));
    }

}
